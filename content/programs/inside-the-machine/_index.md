+++
archiveId = "machinefm-inside"
artwork = "artwork.jpg"
category = "Music"
copyright = "2013 - 2015 Nicholas Young"
date = "2013-07-01"
draft = false
explicit = false
layout = "show"
show = "inside-the-machine"
title = "Inside the Machine"
outputFormats = ["rss", "jsonfeed"]
+++
Your guide to the world of independent music. Nicholas Young gets the raw stories behind today's rising stars.
