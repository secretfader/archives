+++
archiveId = "chickhabit"
artwork = "artwork.jpg"
author = "Ashly Dalene"
category = "Society & Culture"
copyright = "2015 Ashly Dalene"
date = "2015-04-09"
description = "Honest, raw conversations from women in the arts. This is the female perspective you've been looking for."
draft = false
explicit = false
guid = "7a20175b-bd1a-4c23-913d-ed68ba4ad6a2"
layout = "show"
show = "chick-habit"
subcategory = "Personal Journals"
title = "Chick Habit"
outputFormats = ["rss", "jsonfeed"]
+++
Created by Ashly Dalene in early 2015, Chick Habit explores the often significant (but overlooked) contribution women make to the Chicago music scene and around the world.
