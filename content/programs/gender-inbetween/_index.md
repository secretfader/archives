+++
archiveId = "machinefm-genderinbetween"
date = "2015-05-10"
draft = false
guid = "06020182-3fb9-41ef-bbc6-f94816dbc23d"
layout = "show"
show = "gender-inbetween"
title = "Gender Inbetween"
outputFormats = ["rss", "jsonfeed"]
+++
An ongoing project documenting the personal histories and stories of individuals living both within and outside of the gender binary.
