+++
archiveId = "machinefm-dispatch"
artwork = "artwork.jpg"
author = "Nicholas Young & Joshua Wentz"
category = "Arts"
copyright = "2013 - 2015 Joshua Wentz & Nicholas Young"
date = "2013-07-01"
description = "A discussion at the intersection of art, culture, and commerce."
draft = false
explicit = false
guid = "982390ac-b681-40bc-8db6-d6a4694c67bc"
layout = "show"
show = "dispatch"
subcategory = "Design"
title = "Dispatch"
outputFormats = ["JSONFeed"]
+++
Co-created in 2013 by Joshua Wentz and Nicholas Young, Dispatch touches almost every aspect of the modern artist's career. Wentz, a label-owner and musician, brings a unique perspective from Chicago's underground. 
