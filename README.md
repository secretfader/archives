# Archives

Past works of art and media created by Nicholas Young (a.k.a. "Fader").

## License

(C) Copyright 2019 Nicholas Young. All rights reserved. Released under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License][LICENSE].

[LICENSE]: LICENSE
